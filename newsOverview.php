<?php
$title = "Manage news objects";
include './controller/newsController.php';
$newsController = new newsController();

$content = $newsController->createOverviewTable();

if(isset($_GET["delete"]))
{
    $newsController->deleteNews($_GET["delete"]);
}

include './template.php';
?>
