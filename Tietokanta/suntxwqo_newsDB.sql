-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 20, 2018 at 08:08 AM
-- Server version: 10.1.24-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `suntxwqo_newsDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `releasedate` varchar(255) DEFAULT NULL,
  `writer` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `article` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `releasedate`, `writer`, `image`, `article`) VALUES
(19, '9 niksiÃ¤ helpompaan arkeen', '19.2', 'A.H', 'viinipullo.jpg', 'Yksi kÃ¤tevimmistÃ¤ nikseistÃ¤ viinipullon avaaminen ilman avaajaa. Kun pullon kaulaa lÃ¤mmitetÃ¤Ã¤n kevyesti sytkÃ¤rillÃ¤, korkki alkaa nousemaan pullon sisÃ¤ltÃ¤ kuin itsestÃ¤Ã¤n. Toisessa hyÃ¶dyllisessÃ¤ niksissÃ¤ kiivin hedelmÃ¤liha kaavitaan kuoren sisÃ¤ltÃ¤ vatkaimella.'),
(20, 'Dutch activate hard Brexit plan and blame a lack of clarity from the UK', 'Tuesday 20 February 2018', 'Faisal Islam', 'skynews-rotterdam-port_4235663.jpg', 'The Dutch government has linked its decision to activate what it refers to as a "hard Brexit" plan for customs to "divisions within the British Conservative Party" and a "remaining lack of clarity" from the UK.\r\n\r\n'),
(22, 'Syria war: Scores of civilians killed in Eastern Ghouta strikes', '20.2.2018 12:06', 'BBC NEWS', '_100099615_mediaitem100099614.png', 'Bombardments by Syrian government forces have killed at least 100 people including 20 children in the rebel-held Eastern Ghouta area outside Damascus, rescue teams and monitors say.\r\n\r\nThis would make Monday one of the deadliest days for the district since it came under siege in 2013.\r\n\r\nThe Syria Civil Defence, also known as the White Helmets, said bombs were continuing to fall on Tuesday morning.'),
(23, 'Britain wont be plunged into a Mad Max-style world after Brexit, says David Davis', '15.2.2018', 'Andrew G', 'brexit.jpg', 'David Davis has promised that Britain would be plunged into a Mad Max-style world borrowed from dystopian fiction after we leave the EU.\r\n\r\nThe Brexit secretary will vow at a speech on Tuesday that the UK will not enter a race to the bottom on rules and regulations after we quit the bloc.\r\n\r\n\r\nIn a keynote speech in Vienna, Mr Davis will say the UK wants to maintain close, even-handed co-operation with EU regulatory authorities.\r\n\r\nAnd he will say a common commitment to high standards should ensure trade with the EU remains as frictionless as possible after Brexit.'),
(24, 'testi', 'asdasdasdsa', 'asdasdadasd', 'viinipullo.jpg', 'asdasdadada'),
(25, 'asdasdasdasd', 'asdasdasdas', 'asdasdasd', 'tumblr_p15tb8GZGG1rt8loro1_1280.png', 'asdasdas');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`) VALUES
(1, 'santeri', '$2y$10$Wweze9ZmIjKmNBpQGDe6ue5g0u93wTKuBnesMJ.4S44HKF6EB0B4e', '2018-02-18 08:11:16'),
(2, 'crazyfin', '$2y$10$uimPw72ckNyNsN4xhn3XKe56Ger7SL.G7uD2C30cwCBs0PWY2X2cC', '2018-02-19 15:09:49'),
(3, 'santtu123', '$2y$10$BkwdcsJL9sjTrVsmxath4Ol.V6NoJsW/t98ccJ134pb8ulYBF8cLy', '2018-02-20 06:44:35');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
