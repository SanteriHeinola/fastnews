<?php
require './controller/newsController.php';
$newsController = new newsController();

$title = "Add a new news article";


if(isset($_GET["update"]))
{
    $news = $newsController->getNewsById($_GET["update"]);

    $content ="<form action='' method='post'>
    <fieldset>

        <legend>Add a new article</legend>
        <label for='name'>Name: </label>
        <input type='text' class='inputField' name='txtName' value='$news->name'  /><br/>

        <label for='releasedate'>Date: </label>
        <input type='text' class='inputField' name='txtReleasedate' value='$news->releasedate'/><br/>

        <label for='writer'>Writer: </label>
        <input type='text' class='inputField' name='txtWriter' value='$news->writer'/><br/>

        <label for='image'>Image: </label>
        <select class='inputField' name='ddlImage'>"
        .$newsController->getImages().
        "</select></br>

        <label for='article'>Article: </label>
        <textarea cols='70' rows='12' name='txtArticle'>$news->article</textarea></br>

        <input type='submit' value='Submit'>
    </fieldset>
</form>";
}
 else
{
    $content ="<form action='' method='post'>
    <fieldset>
        <legend>Add a new Article</legend>
        <label for='name'>Name: </label>
        <input type='text' class='inputField' name='txtName' /><br/>

        <label for='releasedate'>Date: </label>
        <input type='text' class='inputField' name='txtReleasedate' /><br/>

        <label for='writer'>Writer: </label>
        <input type='text' class='inputField' name='txtWriter'/><br/>

        <label for='image'>Image: </label>
        <select class='inputField' name='ddlImage'>"
        .$newsController->getImages().
        "</select></br>

        <label for='article'>Article: </label>
        <textarea cols='70' rows='12' name='txtArticle'></textarea></br>

        <input type='submit' value='Submit'>
    </fieldset>
</form>";
}


if(isset($_GET["update"]))
{
    if(isset($_POST["txtName"]))
    {
        $newsController->updateNews($_GET["update"]);
    }
}
else
{
    if(isset($_POST["txtName"]))
    {
        $newsController->insertNews();
    }
}

include './template.php';
?>
