<?php

require 'controller/newsController.php';
$newsController = new newsController();

if(isset($_POST['writer']))
{
    //Fill page with news of the selected writer
    $newsTables = $newsController->createNewsTables($_POST['writer']);
}
else
{
    //Page is loaded for the first time, no writer selected -> Fetch all writers
    $newsTables = $newsController->createNewsTables('%');
}

$title = 'News overview';
$content = $newsController->createNewsDropdownList(). $newsTables;

include 'template.php';
 ?>
