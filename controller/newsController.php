<script>
//Display a confirmation box when trying to delete an object
function showConfirm(id)
{
    // build the confirmation box
    var c = confirm("Are you sure you wish to delete this item?");

    // if true, delete item and refresh
    if(c)
        window.location = "newsOverview.php?delete=" + id;
}
</script>

<?php
require ("model/newsModel.php");

//Contains non-database related function for the News page
class newsController
{
  function createOverviewTable() {
      $result = "
          <table class='overViewTable'>
              <tr>
                  <td></td>
                  <td></td>
                  <td><b>Id</b></td>
                  <td><b>Name</b></td>
                  <td><b>Release Date</b></td>
                  <td><b>Writer</b></td>
              </tr>";

      $newsArray = $this->getNewsByWriter('%');

      foreach ($newsArray as $key => $value) {
          $result = $result .
                  "<tr>
                      <td><a href='newsAdd.php?update=$value->id'>Update</a></td>
                      <td><a href='#' onclick='showConfirm($value->id)'>Delete</a></td>
                      <td>$value->id</td>
                      <td>$value->name</td>
                      <td>$value->releasedate</td>
                      <td>$value->writer</td>
                  </tr>";
      }

      $result = $result . "</table>";
      return $result;
  }
  function createNewsDropdownList()
    {
        $newsModel = new newsModel();
        $result = "<form action = '' method = 'post' width = '200px'>
                    Please select a writer:
                    <select name = 'writer' >
                        <option value = '%' >All</option>
                        " . $this->createOptionValues($newsModel->getNewsWriter()) .
                "</select>
                     <input type = 'submit' value = 'Search' />
                    </form>";

        return $result;
    }

    function createOptionValues(array $valueArray)
    {
        $result = "";

        foreach ($valueArray as $value) {
            $result = $result . "<option value='$value'>$value</option>";
        }

        return $result;
    }

    function createNewsTables($writer)
    {
        $newsModel = new newsModel();
        $newsArray = array_reverse($newsModel->getNewsByWriter($writer));
        $result = "";

        //Generate a newsTable for each newsEntity in array
        foreach ($newsArray as $key => $news) {
            $result = $result .
                    "<table class = 'newsTable'>
                        <tr>
                            <th rowspan='6' width = '150px' ><img runat = 'server' src = 'images/Coffee/$news->image' /></th>
                            <th width = '75px' >Name: </th>
                            <td>$news->name</td>
                        </tr>

                        <tr>
                            <th>Date: </th>
                            <td>$news->releasedate</td>
                        </tr>

                        <tr>
                            <th>Writer: </th>
                            <td>$news->writer</td>
                        </tr>
                        <div class='article'>
                            <th>
                            <td>$news->article</td>
                            </th>
                        </div>
                     </table>";
        }
        //<tr>
        //    <th>Article: </th>
        //    <td>$news->article</td>
        //</tr>
        return $result;
    }
    //Returns list of files in a folder.
    function getImages()
    {
        //Select folder to scan
        $handle = opendir("images/Coffee");

        //Read all files and store names in array
        while ($image = readdir($handle)) {
            $images[] = $image;
        }

        closedir($handle);

        //Exclude all filenames where filename length < 3
        $imageArray = array();
        foreach ($images as $image) {
            if (strlen($image) > 2) {
                array_push($imageArray, $image);
            }
        }

        //Create <select><option> Values and return result
        $result = $this->createOptionValues($imageArray);
        return $result;
    }

    function updateNews($id) {
        $name = $_POST["txtName"];
        $releasedate = $_POST["txtReleasedate"];
        $writer = $_POST["txtWriter"];
        $image = $_POST["ddlImage"];
        $article = $_POST["txtArticle"];

        $news = new newsEntity($id, $name, $releasedate, $writer, $image, $article);
        $newsModel = new newsModel();
        $newsModel->updateNews($id, $news);
    }

    function deleteNews($id)
    {
        $newsModel = new newsModel();
        $newsModel->deleteNews($id);
        
    }
    //</editor-fold>
    //<editor-fold desc="Get Methods">
    function insertNews()
    {
        $name = $_POST["txtName"];
        $releasedate = $_POST["txtReleasedate"];
        $writer = $_POST["txtWriter"];
        $image = $_POST["ddlImage"];
        $article = $_POST["txtArticle"];

        $news = new newsEntity(-1, $name, $releasedate, $writer, $image, $article);
        $newsModel = new newsModel();
        $newsModel->insertNews($news);
    }

    function getNewsById($id)
    {
        $newsModel = new newsModel();
        return $newsModel->getNewsById($id);
    }

    function getNewsByWriter($writer)
    {
          $newsModel = new newsModel();
          return $newsModel->getNewsByWriter($writer);
    }
    function getNewsWriter()
    {
        $newsModel = new newsModel();
        return $newsModel->getNewsWriter();
    }
}
?>
