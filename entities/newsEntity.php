<?php
class newsEntity
{
    public $id;
    public $name;
    public $releasedate;
    public $writer;
    public $image;
    public $article;

    function __construct($id, $name, $releasedate, $writer, $image, $article) {
        $this->id = $id;
        $this->name = $name;
        $this->releasedate = $releasedate;
        $this->writer = $writer;
        $this->image = $image;
        $this->article = $article;
    }
}
?>
