<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="shortcut icon" href="favicon.ico" >
  <title><?php echo $title; ?></title>
  <link rel="stylesheet" type="text/css" href="styles/stylesheet.css" />
</head>

<body>
    <div id="wrapper">
        <a href="news.php"><div id="banner">
        </div></a>

        <nav id="navigation">
            <ul id="nav">

              <li><a href="news.php">News</a></li>

              <?php
              // Initialize the session
              session_start();

              // If session variable is not set it will redirect to login page
              if(!isset($_SESSION['username']) || empty($_SESSION['username'])) {
                print('<li><a href="login.php">Login</a></li>');
              } else {
                print('<li><a href="management.php">Management</a></li>');
                print('<li><a href="logout.php">Logout</a></li>');
              }
              ?>
            </ul>
        </nav>

        <div id="content_area">
            <?php echo $content; ?>
        </div>

        <div id="sidebar">
          <form>

           <button type="button" name="display" id="display">Login tiedot näyttöä varten</button>
          </form>

          <div id="result">&nbsp;</div>

          <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
          <script type="text/javascript">
          $("#display").click(function(e) {
            e.preventDefault();
              $.ajax({
                url: 'display.php',
                type: 'GET',
                dataType: "html",
                data: {
                   "id": "Salasana: AttelcaC1295 <br>Käyttäjänimi: santeri <br>Tämä on tehty ajaxilla",
                },
                success: function(data) {
                //called when successful
                $('#result').html(data);
                },
                error: function(e) {
                //called when there is an error
                //console.log(e.message);
                }
              });

          });
          </script>
        </div>

        <footer>
            <p>All rights reserved</p>
        </footer>
    </div>
</body>

</html>
