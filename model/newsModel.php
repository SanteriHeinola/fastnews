<?php
require ("entities/newsEntity.php");

//sisältää dabase koodin uutissivua varten
class newsModel
  {
    //hakee kaikki uutisten kirjoittajat ja palauttaa ne arrayna
    function getNewsWriter()
    {
      require ('credentials.php');
      //Avaa yhteyden ja SELECT database
      $conn = new mysqli($host,$user,$passwd,$database);
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
      $result = $conn->query("SELECT * FROM news");
      $writer = array();

      //hakee datan databasesta
      while($row = $result->fetch_assoc())
      {
        array_push($writer, $row["writer"]);
      }

      //Sulkee yhteyden ja palauttaa tulokset
      $conn->close();
      return $writer;
    }
    //hakee kaikki newsEntity objektit ja palauttaa ne arrayna
    function getNewsByWriter($writer)
    {
      //Avaa yhteyden ja SELECT database
      require ('credentials.php');
      $conn = new mysqli($host,$user,$passwd,$database);
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
      $query = "SELECT * FROM news WHERE writer LIKE '$writer'";
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }

      $result = $conn->query($query);
      $newsArray = array();

      //hakee datan databasesta
      while($row = $result->fetch_array())
      {
        //print_r($row);
        $id=$row[0];
        $name = $row[1];
        $releasedate = $row[2];
        $writer = $row[3];
        $image = $row[4];
        $article = $row[5];

        //tekee uutisobjektit ja tallettaa ne listoiksi
      $news = new newsEntity ($id, $name, $releasedate, $writer, $image, $article);
        array_push($newsArray, $news);
      }
      //Sulkee yhteyden ja palauttaa tulokset
      $conn->close();

      return $newsArray;

    }
    function getNewsByid($id)
    {
      //Avaa yhteyden ja SELECT database
      require ('credentials.php');
      $conn = new mysqli($host,$user,$passwd,$database);
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
      $query = "SELECT * FROM news WHERE id = $id";
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
      $result = $conn->query($query);

      //hakee datan databasesta
      while($row = $result->fetch_array())
      {
        //print_r($row);
        $name = $row[1];
        $releasedate = $row[2];
        $writer = $row[3];
        $image = $row[4];
        $article = $row[5];

        //tekee uutisobjektit ja tallettaa ne listoiksi
      $news = new newsEntity ($id, $name, $releasedate, $writer, $image, $article);
      }
      //Sulkee yhteyden ja palauttaa tulokset
      $conn->close();
      return $news;
    }
    function insertNews(newsEntity $news)
    {
      /*
      require ('credentials.php');
      $conn = new mysqli($host,$user,$passwd,$database);
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }*/
      $query = "
        INSERT INTO
          news (
              name,
              releasedate,
              writer,
              image,
              article
          )
          VALUES (
              '" . $news->name . "',
              '" . $news->releasedate . "',
              '" . $news->writer . "',
              '" . $news->image . "',
              '" . $news->article . "'
          )
      ";
  $this->performQuery($query);
  header("Location: newsOverview.php");

      /*$conn->query($query) or die('Error : ('. $conn->errno .') '. $conn->error);
      $conn->close();*/
        /*$query = sprintf("INSERT INTO news
                          (name, type, price,roast,country,image,review)
                          VALUES
                          ('%s','%s','%s','%s','%s','%s','%s')",
                mysql_real_escape_string($news->name),
                mysql_real_escape_string($news->type),
                mysql_real_escape_string($news->price),
                mysql_real_escape_string($news->roast),
                mysql_real_escape_string($news->country),
                mysql_real_escape_string("Images/news/" . $news->image),
                mysql_real_escape_string($news->review));
        $this->PerformQuery($query);*/
    }
    function updateNews($id, newsEntity $news)
    {
      $query = "
        UPDATE
          news
        SET
          name='" . $news->name . "',
          releasedate='" . $news->releasedate . "',
          writer='" . $news->writer . "',
          image='" . $news->image . "',
          article='" . $news->article . "'
        WHERE
          id='" . $news->id . "'
      ";
      /*$query = sprintf("UPDATE news
                        SET name = '%s',
                        releasedate = '%s',
                        writer = '%s',
                        image = '%s',
                        article = '%s'
                        WHERE id = $id",
              mysql_real_escape_string($news->name),
              mysql_real_escape_string($news->releasedate),
              mysql_real_escape_string($news->writer),
              mysql_real_escape_string("images/news/" . $news->image),
              mysql_real_escape_string($news->article));*/

      $this->performQuery($query);
      header("Location: newsOverview.php");
    }

    function deleteNews($id)
    {
      $query = "DELETE FROM news WHERE id = $id";
      $this->performQuery($query);
      header("Location: newsOverview.php");
    }

    function performQuery($query)
    {
      require ('credentials.php');
      $conn = new mysqli($host,$user,$passwd,$database);
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }


      $conn->query($query) or die('Error : ('. $conn->errno .') '. $conn->error);
      $conn->close();
    }
  }
?>
